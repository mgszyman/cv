(function ($) {

    var menuItems = $('#navbar-items').find('li');
    var navItems = $('#navbar').find('li');

    menuItems.click(function (e) {
        e.preventDefault();

        var el = $(this);
        var anchor = el.find('a');
        menuItems.removeClass('active')
        el.addClass('active');

        $('body').scrollTo(anchor.attr('href'), { duration: 'slow' });

    });

    navItems.click(function () {
        var el = $(this);
        var anchor = el.find('a');
        if (!anchor.hasClass('dropdown-toggle')) {
            $('.navbar-toggle:visible').click();
        }
    });

})(jQuery);